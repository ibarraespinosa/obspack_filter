####################################################################
#' Obspack filter
#'
#' This script reads generates an receptor files file based
#' on the file receptor_test.txt
#' 
#' For more information, read README.md
#' @author: Sergio Ibarra-Espinosa (sergio.ibarra-espinosa@noaa.gov)
####################################################################

library(data.table)
file.remove(list.files(path = "receptors/", full.names = T))
x <- list.files(path = "tmp/",
                pattern = "NORTH_AMERICA", 
                full.names = T)

na <- list.files(path = "tmp/",
                 pattern = "NORTH_AMERICA", 
                 full.names = F)
# na <- gsub(".csv", "", na)
dt <- lapply(x, fread)

nt <- lapply(dt, names)

# check for name method ####
for(i in seq_along(nt)){
  if(sum(names(dt[[i]]) == "method") == 0) {
    print(i)
    dt[[i]]$method <- NA
  }
} 


# check for name elevation ####
for(i in seq_along(nt)){
  if(sum(names(dt[[i]]) == "elevation") == 0){
    print(i)
    dt[[i]]$elevation <- 0
  }
} 

# check names intersected ####
nt <- lapply(dt, names)
nt12 <- intersect(nt[[1]], nt[[1]])
nt123 <- intersect(nt12, nt[[3]])
nt1234 <- intersect(nt123, nt[[4]])
nt12345 <- intersect(nt1234, nt[[5]])

for( i in seq_along(dt)) print(min(dt[[i]]$local_time))

for(i in seq_along(dt)) {
  dt[[i]]$file <- gsub(pattern = ".csv", replacement = "", x = na[i])
}

dx <- rbind(
  dt[[1]][, c(nt12345, "file"), with = F],
  dt[[2]][, c(nt12345, "file"), with = F],
  dt[[3]][, c(nt12345, "file"), with = F],
  dt[[4]][, c(nt12345, "file"), with = F],
  dt[[5]][, c(nt12345, "file"), with = F]
)

names(dx)

dx$start_time <- NULL
dx$midpoint_time <- NULL

#fwrite(dx, "receptors/all.csv")
dim(dx) # 12535    60
dx$minutes_localtime <- minute(dx$local_time)
dx$seconds_localtime <- second(dx$local_time)
summary(dx$local_time)
################################################
# formating files according /work/noaa/co2/Lei.Hu/HYSPLIT_NA/receptors/%s
# It seems that the column names are not important, because of #

# ccg_aircraft_2020.txt: 

# sample_site_code 
# sample_year sample_month sample_day sample_hour sample_minute sample_seconds 
# sample_id sample_method sample_latitude sample_longitude 

# ccg_flask_2020.txt:

# sample_site_code 
# sample_year sample_month sample_day sample_hour sample_minute sample_seconds 
# sample_id sample_method sample_latitude sample_longitude 
# sample_altitude sample_elevation event_number


# ccg_surface_2020.txt

# sample_site_code 
# sample_year sample_month sample_day sample_hour sample_minute sample_seconds 
# sample_id sample_method sample_latitude sample_longitude 
# sample_altitude sample_elevation event_number

# hats_flask_2020.txt
# site year month day hour minute lat lon event agl

################################################
# => merge surface with tower and generate two files:
# ch4_surface_2020.txt and ch4_aircraft_2020.txt: 

add_0 <- function(x) ifelse(nchar(x) < 2, paste0(0, x), x)
dx$month_localtime <- add_0(dx$month_localtime)
dx$day_localtime <- add_0(dx$day_localtime)
dx$hour_localtime <- add_0(dx$hour_localtime)
dx$minutes_localtime <- add_0(dx$month_localtime)
dx$seconds_localtime <- add_0(dx$seconds_localtime)


dx$year_end <- add_0(dx$year_end)
dx$month_end <- add_0(dx$month_end)
dx$day_end <- add_0(dx$day_end)
dx$hour_end <- add_0(dx$hour_end)
dx$minute_end <- add_0(dx$minute_end)
dx$second_end <- add_0(dx$second_end)

unique(dx$file)
# receptor_test.txt

# sample_site_code 
# sample_year
# sample_month 
# sample_day 
# sample_hour 
# sample_minute 
# sample_seconds 
# sample_id 
# sample_method 
# sample_latitude 
# sample_longitude 
# sample_altitude 
# sample_elevation 
# event_number

# rec_s = rec[[
# "sample_site_code",
# "sample_year",
# "sample_month",
# "sample_day",
# "sample_hour",
# "sample_minute",
# "sample_latitude",
# "sample_longitude", 
# "event_number"]]

# Lei file: run_hysplit_nams_main.py
# U8: 64bit unsigned integer
# int: integer
# float: real
# U1:  unsigned 8-bit integer


# flask:
# 43     "U8","int", "int", "int", "int", "int", "int","U8","U1","float","float", "float", "float", "U6"])
# 44     "sample_site_code", "sample_year", "sample_month", "sample_day", "sample_hour","sample_minute","sample_latitude","sample_longitude", "event_number"
# hatsflask
# 53     "U3","int", "int", "int", "int", "int", "float","float","int","float"



# 2. tower ####
tower <- dx[file %chin% c("tower_insitu_2020_NORTH_AMERICA")]

names(tower)
grep(pattern = "site", x = names(tower), value = T)

grep(pattern = "sample", x = names(tower), value = T)

unique(tower$site_code)
ch4_tower_2020 <- tower[, 
                        c("site_code",
                          "year",
                          "month",
                          "day",
                          "hour",
                          "minute",
                          "second",
                          "unique_sample_location_num", # sample_id
                          "method", # sample_method
                          "latitude", # sample_latitude
                          "longitude", # sample_longitude
                          "altitude", # sample_altitude
                          "elevation", # sample_elevation
                          "mole_fraction",
                          "decimal_year",
                          "agl",
                          "asl",
                          "what", #if missing agl, asl (site elevation)
                          "year_end",
                          "month_end",
                          "day_end",
                          "hour_end",
                          "second_end")] # event_number not found
ch4_tower_2020$method <- as.character(ch4_tower_2020$method)
ch4_tower_2020$method <- "NAN"
ch4_tower_2020$event_number <- "NAN"
ch4_tower_2020[is.na(ch4_tower_2020)] <- "NAN"
names(ch4_tower_2020)[1] <- "# site_code"


fwrite(ch4_tower_2020, 
       "receptors/ch4_tower_2020.txt", 
       sep = " ")


# 1. surface ####
surface <- dx[file %chin% c("surface_insitu_2020_NORTH_AMERICA")]

names(surface)
grep(pattern = "site", x = names(surface), value = T)

grep(pattern = "sample", x = names(surface), value = T)
grep(pattern = "method", x = names(surface), value = T)

unique(surface$site_code)
ch4_surface_2020 <- surface[, 
                            c("site_code",
                              "year",
                              "month",
                              "day",
                              "hour",
                              "minute",
                              "second",
                              "unique_sample_location_num", # sample_id
                              "method", # sample_method
                              "latitude", # sample_latitude
                              "longitude", # sample_longitude
                              "altitude", # sample_altitude
                              "elevation", # sample_elevation
                              "mole_fraction",
                              "decimal_year",
                              "agl",
                              "asl",
                              "what", #if missing agl, asl (site elevation)
                              "year_end",
                              "month_end",
                              "day_end",
                              "hour_end",
                              "second_end")] # event_number not found
ch4_surface_2020$method <- as.character(ch4_surface_2020$method)
ch4_surface_2020$method <- "NAN"
ch4_surface_2020$event_number <- "NAN"
ch4_surface_2020[is.na(ch4_surface_2020)] <- "NAN"
names(ch4_surface_2020)[1] <- "# site_code"

fwrite(ch4_surface_2020, 
       "receptors/ch4_surface_2020.txt", 
       sep = " ")



# 3. aircraft ####
aircraft <- dx[file %chin% c("aircraft_insitu_2020_NORTH_AMERICA")]

names(aircraft)
grep(pattern = "site", x = names(aircraft), value = T)

grep(pattern = "sample", x = names(aircraft), value = T)

unique(aircraft$site_code)
ch4_aircraft_2020 <- aircraft[, 
                              c("site_code",
                                "year",
                                "month",
                                "day",
                                "hour",
                                "minute",
                                "second",
                                "unique_sample_location_num", # sample_id
                                "method", # sample_method
                                "latitude", # sample_latitude
                                "longitude", # sample_longitude
                                "altitude", # sample_altitude
                                "elevation", # sample_elevation
                                "mole_fraction",
                                "decimal_year",
                                "agl")] # event_number not found
ch4_aircraft_2020$method <- as.character(ch4_aircraft_2020$method)
ch4_aircraft_2020$method <- "NAN"
ch4_aircraft_2020$event_number <- "NAN"
ch4_aircraft_2020[is.na(ch4_aircraft_2020)] <- "NAN"
names(ch4_aircraft_2020)[1] <- "# site_code"

# change after 45:run_hysplit_nams_main.py
# "sample_site_code", 
# "sample_year", "sample_month", "sample_day", 
# "sample_hour","sample_minute",
# "sample_latitude","sample_longitude", "event_number"

# change names accordingly
nas <- c("# sample_site_code",
         "sample_year",
         "sample_month",
         "sample_day",
         "sample_hour",
         "sample_minute",
         "sample_seconds",
         "sample_id",
         "sample_method",
         "sample_latitude",
         "sample_longitude",
         "sample_altitude",
         "sample_elevation",
         "mole_fraction",
         "decimal_year",
         "event_number",
         "agl"
)

names(ch4_aircraft_2020) <- nas

print(paste0("aircraft: ", nrow(ch4_aircraft_2020)))

ch4_aircraft_2020 <- unique(ch4_aircraft_2020)

print(paste0("aircraft: ", nrow(ch4_aircraft_2020)))

ch4_aircraft_2020$mole_fraction <- NULL
ch4_aircraft_2020$decimal_year <- NULL
ch4_aircraft_2020$agl <- NULL

fwrite(ch4_aircraft_2020, 
       "receptors/ch4_aircraft_2020.txt", 
       sep = " ")


# 4. aircore ####
aircore <- dx[file %chin% c("aircore_2020_NORTH_AMERICA")]

names(aircore)
grep(pattern = "site", x = names(aircore), value = T)

grep(pattern = "sample", x = names(aircore), value = T)

unique(aircore$site_code)
ch4_aircore_2020 <- aircore[, 
                            c("site_code",
                              "year",
                              "month",
                              "day",
                              "hour",
                              "minute",
                              "second",
                              "unique_sample_location_num", # sample_id
                              "method", # sample_method
                              "latitude", # sample_latitude
                              "longitude", # sample_longitude
                              "altitude", # sample_altitude
                              "elevation", # sample_elevation
                              "mole_fraction",
                              "decimal_year",
                              "agl",
                              "asl",
                              "what", #if missing agl, asl (site elevation)
                              "year_end",
                              "month_end",
                              "day_end",
                              "hour_end",
                              "second_end")] # event_number not found
ch4_aircore_2020$method <- as.character(ch4_aircore_2020$method)
ch4_aircore_2020$method <- "NAN"
ch4_aircore_2020$event_number <- "NAN"
ch4_aircore_2020[is.na(ch4_aircore_2020)] <- "NAN"
names(ch4_aircore_2020)[1] <- "# site_code"
fwrite(ch4_aircore_2020, 
       "receptors/ch4_aircore_2020.txt", 
       sep = " ")



# 4. flask ####
flask <- dx[file %chin% c("flask_NON_NOAA_2020_NORTH_AMERICA")]

names(flask)
grep(pattern = "site", x = names(flask), value = T)

grep(pattern = "sample", x = names(flask), value = T)

unique(flask$site_code)
ch4_flask_2020 <- flask[, 
                        c("site_code",
                          "year",
                          "month",
                          "day",
                          "hour",
                          "minute",
                          "second",
                          "unique_sample_location_num", # sample_id
                          "method", # sample_method
                          "latitude", # sample_latitude
                          "longitude", # sample_longitude
                          "altitude", # sample_altitude
                          "elevation", # sample_elevation
                          "mole_fraction",
                          "decimal_year",
                          "agl",
                          "asl",
                          "what", #if missing agl, asl (site elevation)
                          "year_end",
                          "month_end",
                          "day_end",
                          "hour_end",
                          "second_end")] # event_number not found
ch4_flask_2020$method <- as.character(ch4_flask_2020$method)
ch4_flask_2020$method <- "NAN"
ch4_flask_2020$event_number <- "NAN"
ch4_flask_2020[is.na(ch4_flask_2020)] <- "NAN"
names(ch4_flask_2020)[1] <- "# site_code"

fwrite(ch4_flask_2020, 
       "receptors/ch4_NON_NOAA_flask_2020.txt", 
       sep = " ")

