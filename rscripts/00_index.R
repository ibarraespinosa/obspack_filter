####################################################################
#' Obspack filter
#'
#' This script reads generates an index.csv file based
#' on the obspack path
#' 
#' Fot more information, read READM.md
#' @author: Sergio Ibarra-Espinosa (sergio.ibarra-espinosa@noaa.gov)
####################################################################
library(data.table)
print("First argument it the path to obspack text files")


#args <- "../obspack_ch4_1_GLOBALVIEWplus_v4.0_2021-10-14/data/txt/"
args = commandArgs(trailingOnly=TRUE)

if(length(args) == 0) {
      stop("Run as 'Rscript path-to-obspack/data/text' or in interactive mode")
}

x <- list.files(args[1],
                full.names = T)

na <- list.files(args[1],
                 full.names = F)

# create index ####
# the idea
index <- data.table(id = x, name = na)
index[grepl(pattern = "aircraft-pfp", x = x), 
      sector := "aircraft-pfp"]

index[grepl(pattern = "aircraft-insitu", x = x), 
      sector := "aircraft-insitu"]

index[grepl(pattern = "surface-insitu", x = x), 
      sector := "surface-insitu"]

index[grepl(pattern = "tower-insitu", x = x), 
      sector := "tower-insitu"]

index[grepl(pattern = "aircore", x = x), 
      sector := "aircore"]

index[grepl(pattern = "surface-pfp", x = x), 
      sector := "surface-pfp"]

index[grepl(pattern = "shipboard-insitu", x = x), 
      sector := "shipboard-insitu"]

index[grepl(pattern = "flask", x = x), 
      sector := "flask"]

index[is.na(sector)]

print(paste0("Dimension of data.table files"))
print(dim(index))

# function to extract last n characters
sr <- function(x, n){
  substr(x, nchar(x)-n+1, nchar(x))
}

# only in id with character magl
index[grepl(pattern = "magl", x = x), 
      agl := sr(id, 11)]

# This line replaces removes the characters magl.txt
index$agl <- gsub("magl.txt", "", index$agl)
# check
index
# Then it transform in numeric, for instance, -11
# then get the absolute number and now we have magl
index$magl <- abs(as.numeric(index$agl))


fwrite(index, "index.csv")

# check for duplicates ####
x_air_pfp <- grep(pattern = "aircraft-pfp", x = x, value = T)
x_air_insitu <- grep(pattern = "aircraft-insitu", x = x, value = T)
x_surface_insitu <- grep(pattern = "surface-insitu", x = x, value = T)
x_aircore <- grep(pattern = "aircore", x = x, value = T)
x_surface_pfp <- grep(pattern = "surface-pfp", x = x, value = T)
x_tower_insitu <- grep(pattern = "tower-insitu", x = x, value = T)
x_shipboard_insitu <- grep(pattern = "shipboard-insitu", x = x, value = T)
x_flask <- grep(pattern = "flask", x = x, value = T)


print(paste0("any duplicated?"))

intersect(x_air_pfp, x_air_insitu)
intersect(x_surface_pfp, x_surface_insitu)
