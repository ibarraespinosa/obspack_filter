

dt$long <- dt$longitude
dt$lat <- dt$latitude
dt$mole_fraction <- dt$value*1e9 # to ppb
dt$decimal_year <- dt$time_decimal

inter <- dt[latitude < north &
                  latitude > south &
                  longitude < east &
                  longitude > west]


# Priority site_utc2lst and if it is not available, 
inter$local_time <- inter$timeUTC + as.numeric(inter$site_utc2lst)*60*60

# this change seconds, but let us keep the same original seconds
inter[is.na(local_time), local_time := timeUTC + longitude/15*60*60]#john miller approach

# in this way the solar time keep the original seconds
inter$local_time <- ISOdatetime(year = year(inter$local_time), 
                                month = month(inter$local_time),
                                day = strftime(inter$local_time, "%d"), 
                                hour = hour(inter$local_time), 
                                min = strftime(inter$local_time, "%M"), 
                                sec = second(inter$timeUTC))
# 


inter$lh <- hour(inter$local_time)
unique(inter$lh)
class(inter$lh)

inter$year_localtime <- year(inter$local_time)
inter$month_localtime <- month(inter$local_time)
inter$day_localtime <- as.numeric(as.character(strftime(inter$local_time, 
                                                         "%d")))
inter$hour_localtime <- hour(inter$local_time)

inter$site_elevation <- as.numeric(as.character(inter$site_elevation))
