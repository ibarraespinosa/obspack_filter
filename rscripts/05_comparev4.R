
if(Sys.info()[["sysname"]] != "Windows")library(colorout)
library(terra)
library(raster)
library(ggplot2)
library(data.table)
library(cptcity)
library(ncdf4)

fn <- function(x) {
  magick::image_write(magick::image_trim(magick::image_read(x)), x)
}

# map ####
w <- map_data("world")

f1 <- list.files(path = "nc/sergio",
                 pattern = ".nc",
                 full.names = T,
                 recursive = T)

n1 <- list.files(path = "nc/sergio",
                 pattern = ".nc",
                 full.names = F,
                 recursive = T)
n1


f2 <- gsub(pattern = "sergio", replacement = "lei", x = f1)

f2

# part3d ####
nc1  <- nc_open(f1[1])
nc1


n3d = ncvar_get(nc1, "part3dnames")

dt <- data.table::rbindlist(lapply(1:10, function(i) {
  nc1 <- nc_open(f1[i])
  nc2 <- nc_open(f2[i])
  
  dt1 <- as.data.frame(ncvar_get(nc1, "part3d"))
  names(dt1) <- n3d
  dt1$file <- n1[i]
  dt1$nfile <- i
  dt1$who <- "sergio"
  
  dt2 <- as.data.frame(ncvar_get(nc2, "part3d"))
  names(dt2) <- n3d
  dt2$file <- n1[i]
  dt2$nfile <- i
  dt2$who <- "lei"
  
  
  dt <- rbind(dt1, dt2)
  
  dt
}))

dt

names(dt)

dt$file2 <- substr( x = dt$file, start = 8, stop = 26)


# mean values by time
ndt <- names(dt)[6:16]

rbindlist(lapply(seq_along(ndt), function(i) {
dx <- dt[, 
         lapply(.SD, mean, na.rm = T),
         .SDcols = ndt[i],
         by = .(time, who, nfile, file2)]
names(dx)[5] <- "mean"
dx$var <- ndt[i]

dxsd <- dt[, 
         lapply(.SD, sd, na.rm = T),
         .SDcols = ndt[i],
         by = .(time, who, nfile, file2)]
names(dxsd)[5] <- "sd"

dx$sd <- dxsd$sd
dx
})) -> dx



lapply(1:10, function(i) {
  
ggplot(dx[nfile == i], 
       aes(x = time,
           y =mean,
           colour = who,
           shape = who,
           size = who)) +
  geom_point() +
  scale_shape_manual(paste0("file ", i),values = c(1, 4)) +
  scale_size_manual(paste0("file ", i),values = c(2, 3)) +
  scale_colour_brewer(paste0("file ", i),type = "qual", palette = 2) +
  # geom_errorbar(aes(ymin = mean - sd,
                    # ymax = mean + sd))+
  facet_wrap(~var,
             nrow = 2,
             scales = "free_y")+
  labs(x = "time",
       y =  NULL,
       title =NULL)+
  theme_bw() +
  theme(text = element_text(size = 14),
        legend.position = c(0.92, 0.1)) -> p

  png(filename = paste0("figures/part3d_gg_nfile_", i,".png"),
      width = 3500, 
      height =1000, 
      res = 300)
  
  print(p)
  dev.off() 
  fn(paste0("figures/part3d_gg_nfile_", i,".png"))
}) -> lp



