if(Sys.info()[["sysname"]] != "Windows")library(colorout)
library(terra)
library(raster)
library(ggplot2)
library(data.table)
library(cptcity)
library(ncdf4)
library(magick)
fn <- function(x) {
  magick::image_write(magick::image_trim(magick::image_read(x)), x)
}

dx <- fread("csv/foot1.csv.gz")
dx <- dx[dif != 0]

dx
dx[, 
   lapply(.SD, sum, na.rm = T),
   .SDcols = c("sergio", "lei", "dif", "difp"),
   by = .(nl, file)] -> x

x1 <- x[, c("nl", "file", "lei")]
x1$what <- "lei"

x2 <- x[, c("nl", "file", "dif")]
x2$what <- "difference"

dx <- rbind(x1, x2, use.names = F)

# color
col <- cpt(find_cpt("RdBu")[8],
           rev = T)

col <- c(col[1:40], col[60:100])

# limits
rd <- abs(range(dx$lei,na.rm = T))

rd <- c(-max(rd), max(rd))

dx$what <- factor(x = dx$what, levels = c("lei", "difference"))

ggplot(dx,
       aes(x = nl, 
           y = lei,
           colour = lei)) +
  geom_point(size = 2) +
  scale_colour_gradientn("foot1",
                         colours = col,
                         limits = rd) +
  facet_grid(file~what) +
  geom_hline(yintercept = 0, lty = 2)+
  labs(title = "sergio - lei at each timestep",
       x = "timesteps",
       y = expression(paste("ppm / ", mu,
                            ~mol,
                            ~m^-2~s^-1)))  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(2, "cm")) -> p

p


png(filename = "figures/gg_dif_ts.png",
    width = 3500, 
    height = 2500, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_dif_ts.png")



