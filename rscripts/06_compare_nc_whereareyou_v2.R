if(Sys.info()[["sysname"]] != "Windows")library(colorout)
library(terra)
library(raster)
library(ggplot2)
library(data.table)
library(cptcity)
library(ncdf4)

fn <- function(x) {
  magick::image_write(magick::image_trim(magick::image_read(x)), x)
}

fd <- function(x) {
  oldpar <- par(no.readonly = TRUE)
  par(mfrow = c(10,10), mar =c(0,0,0,0))
  for(i in seq_along(x)){
    image(matrix(1:100), col = cpt(x[i]), frame = F, xaxt = "n", yaxt = "n")
    text(x = 50, y = 1, labels = i)
  }
  on.exit(par(oldpar))
}


#args(list.files)
# map ####
w <- map_data("world")

f1 <- list.files(path = "nc/sergio",
                 pattern = ".nc",
                 full.names = T,
                 recursive = T)

n1 <- list.files(path = "nc/sergio",
                 pattern = ".nc",
                 full.names = F,
                 recursive = T)
n1


f2 <- gsub(pattern = "sergio", replacement = "lei", x = f1)

f2
# 
# ff <- function(fx) {
#   rbindlist(pbapply::pblapply(seq_along(fx), function(i){
#     
#     r <- brick(rast(fx[i], "foot1"))
#     
#     rbindlist(lapply(seq_along(names(r)), function(j) {
#       
#       df <-  as.data.frame(rasterToPoints(r[[j]]))
#       
#       names(df)[3] <- "foot"
#       df$id <- 1:nrow(df)
#       df$nfile <- i
#       df$file <- n1[i]
#       df$nlayer <- j
#       df$layer <- names(r)[j]
#       df
#     }))
#     
#   })) 
# }
# 
# dt1 <- ff(f1)
# dt1$who <- "sergio"
# 
# dt2 <- ff(f2)
# dt2$who <- "lei"
# 
# dt <- rbind(dt1, dt2)

# fwrite(dt, "figures/dt_alll.csv.gz")

dt <- fread("figures/dt_alll.csv.gz")
system("ls -ltrh figures/dt_alll.csv.gz")
brks <- rev(log(seq(0, 1, 0.01))*-1)[1:100]
brks <- brks/brks[100]

dt[, 
   sum(foot, na.rm = T),
   by = .(x, y, nfile, who)] -> xx

ggplot() +
  geom_tile(data = xx[V1 > 0 &
                        nfile %in% 1:5],
            aes(x = x,
                y = y,
                fill = V1))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey50") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_grid(who~nfile) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = cpt(rev = T),
                       values = brks) +
  labs(subtitle = expression(paste("ppm/", mu,
                                   ~mol,
                                   ~m^-2~s^-1)),
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_sum_foot_files_1_5.png",
    width = 3000, 
    height = 1000, 
    res = 300,
    bg= "transparent")

print(p)
dev.off() 

fn("figures/gg_sum_foot_files_1_5.png")


ggplot() +
  geom_tile(data = xx[V1 > 0 &
                        nfile %in% 6:10],
            aes(x = x,
                y = y,
                fill = V1))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey50") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_grid(who~nfile) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = cpt(rev = T),
                       values = brks) +
  labs(subtitle = expression(paste("ppm/", mu,
                                   ~mol,
                                   ~m^-2~s^-1)),
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_sum_foot_files_6_10.png",
    width = 3000, 
    height = 1000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_sum_foot_files_6_10.png")

# difference ####

xx1 <- xx[who == "sergio"]
xx1$V1 <- xx[who == "sergio"]$V1 - xx[who == "lei"]$V1

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:30, 71:100)]


secu <- range(xx1$V1)
secu <- c(-max(abs(range(xx1$V1))), max(abs(range(xx1$V1))))

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = V1))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey50") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = expression(paste("ppm/", mu,
                                   ~mol,
                                   ~m^-2~s^-1)),
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_dif_foot_files.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_dif_foot_files.png")



# relative difference ####

xx1 <- xx[who == "sergio"]
xx1$V1 <- (xx[who == "sergio"]$V1 - xx[who == "lei"]$V1)/xx[who == "lei"]$V1

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$V1, na.rm = T)
secu <- c(-1, 1)

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = V1))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Relative difference (sergio - lei)/lei",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_rdif_foot_files.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_rdif_foot_files.png")


# precentage difference ####

xx1 <- xx[who == "sergio"]
xx1$V1 <- (xx[who == "sergio"]$V1 - xx[who == "lei"]$V1)/xx[who == "lei"]$V1*100

col <- cpt(find_cpt("RdBu")[20], rev = T)


secu <- range(xx1$V1, na.rm = T)
secu <- c(-100, 100)

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = V1))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Relative difference (sergio - lei)/lei*100",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_pdif_foot_files.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_pdif_foot_files.png")



# absolute difference timesteps [0] ####
dt[nlayer == 1] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$foot, na.rm = T)

# xx1[foot == 0]$foot <- NA

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei), timestep [0]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_dif_foot_files_ts0.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_dif_foot_files_ts0.png")


# absolute difference timesteps [10] ####
dt[nlayer == 11] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$foot, na.rm = T)

# xx1[foot == 0]$foot <- NA

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei), timestep [10]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_dif_foot_files_ts10.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_dif_foot_files_ts10.png")


# absolute difference timesteps [100] ####
dt[nlayer == 101] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$foot, na.rm = T)

# xx1[foot == 0]$foot <- NA

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei), timestep [100]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_dif_foot_files_ts100.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_dif_foot_files_ts100.png")


# absolute difference timesteps [200] ####
dt[nlayer == 201] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)

col <- cpt(find_cpt("RdBu")[20], rev = T)
# col <- col[c(1:31, 71:100)]


secu <- c(-max(abs(range(xx1$foot, na.rm = T))),
               max(abs(range(xx1$foot, na.rm = T))))
xx1[foot == 0]$foot <- NA
ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei), timestep [200]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_dif_foot_files_ts200.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_dif_foot_files_ts200.png")


# relative difference timesteps [0] ####
dt[nlayer == 1] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)/xx[who == "lei"]$foot

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$foot, na.rm = T)

# xx1[foot == 0]$foot <- NA

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei)/lei, timestep [0]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_rdif_foot_files_ts0.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_rdif_foot_files_ts0.png")


# absolute difference timesteps [0, 10, 100, 200] ####
dt[nlayer == 11] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)/xx[who == "lei"]$foot

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$foot, na.rm = T)

# xx1[foot == 0]$foot <- NA

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei)/lei, timestep [10]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_rdif_foot_files_ts10.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_rdif_foot_files_ts10.png")


# absolute difference timesteps [0, 10, 100, 200] ####
dt[nlayer == 101] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)/xx[who == "lei"]$foot

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- range(xx1$foot, na.rm = T)

# xx1[foot == 0]$foot <- NA

ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col) +
  labs(subtitle = "Difference (sergio - lei)/lei, timestep [100]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_rdif_foot_files_ts100.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_rdif_foot_files_ts100.png")


# absolute difference timesteps [0, 10, 100, 200] ####
dt[nlayer == 201] -> xx

xx1 <- xx[who == "sergio"]
xx1$foot <- (xx[who == "sergio"]$foot - xx[who == "lei"]$foot)/xx[who == "lei"]$foot

col <- cpt(find_cpt("RdBu")[20], rev = T)
col <- col[c(1:31, 71:100)]


secu <- c(-max(abs(range(xx1$foot, na.rm = T))),
          max(abs(range(xx1$foot, na.rm = T))))
xx1[foot == 0]$foot <- NA
ggplot() +
  geom_tile(data = xx1,
            aes(x = x,
                y = y,
                fill = foot))+
  coord_quickmap()+
  geom_polygon(data = w[w$region %in% c("USA", "Canada"), ],
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA, 
               colour = "grey") +
  scale_x_continuous(limits = c(-160, -40)) +
  scale_y_continuous(limits = c(20, 80)) +
  facet_wrap(~nfile, 
             nrow = 2) +
  # ppm per (micromol m-2 s-1)
  scale_fill_gradientn("foot1",
                       colours = col,
                       # values = brks,
                       limits = secu) +
  labs(subtitle = "Difference (sergio - lei)/lei, timestep [200]",
       x = NULL,
       y = NULL)  +
  theme_bw() +
  theme(text = element_text(size = 16),
        legend.key.height = unit(1, "cm"),
        legend.position = "left") -> p

p

png(filename = "figures/gg_rdif_foot_files_ts200.png",
    width = 3000, 
    height = 2000, 
    res = 300)

print(p)
dev.off() 

fn("figures/gg_rdif_foot_files_ts200.png")

