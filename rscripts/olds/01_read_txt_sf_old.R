#' Obspack filter
#'
#' This script reads the text files and process them
#' Then, this script sources 
#' rscripts/source_lx.R: aggregatd(rbind) data
#' rscripts/source_localtime.R: add local time
#' rscripts/source_sfv2.R: Add local time based on intersection with local time
#' 
#' Fot more information, read READM.md
#' @author: Sergio Ibarra-Espinosa (sergio.ibarra-espinosa@noaa.gov)

library(data.table)
library(sf)
library(ggplot2)

dir.create("receptors")
x <- list.files("../obspack_ch4_1_GLOBALVIEWplus_v4.0_2021-10-14/data/txt",
                full.names = T)

na <- list.files("../obspack_ch4_1_GLOBALVIEWplus_v4.0_2021-10-14/data/txt",
                 full.names = F)


# check for duplicates ####
x_air_pfp <- grep(pattern = "aircraft-pfp", x = x, value = T)
x_air_insitu <- grep(pattern = "aircraft-insitu", x = x, value = T)
x_surface_insitu <- grep(pattern = "surface-insitu", x = x, value = T)
x_aircore <- grep(pattern = "aircore", x = x, value = T)
x_surface_pfp <- grep(pattern = "surface-pfp", x = x, value = T)
x_tower_insitu <- grep(pattern = "tower-insitu", x = x, value = T)
x_shipboard_insitu <- grep(pattern = "shipboard-insitu", x = x, value = T)
x_flask <- grep(pattern = "flask", x = x, value = T)

intersect(x_air_pfp, x_air_insitu)
intersect(x_surface_pfp, x_surface_insitu)

# “surface_insitu_2020_US.csv” and “tower_insitu_2020_US.csv
# "aircraft-pfp"
"aircraft-insitu"
"surface-insitu"
"aircore"
# "surface-pfp"     
"tower-insitu"
"shipboard-insitu"
"flask" # NON NOAA



# st_polygon ####
north <- 80
south <- 10
west <- -170
east <- -50

(outer = matrix(c(west, north, 
                  east, north,
                  east, south,
                  west,south,
                  west, north),
                ncol=2, 
                byrow=TRUE))

bb <- st_sf(id = 1, 
            geometry = st_sfc(st_polygon(list(outer))), 
            crs = 4326)

# outersect ####
outersect <- function(x, y) {
  sort(c(setdiff(x, y),
         setdiff(y, x)))
}

# aircraft-insitu ####
x1 <- grep(pattern = "aircraft-insitu", x = x, value = T)
source("rscripts/source_lx.R")
source("rscripts/source_sfv2.R")
inter3 <- inter3[lh %in% 12:18]
# Aircraft only every 20 seconds and plot
inter4 <- inter3[second %in% c(5, 25, 45), ]
dim(inter4) # [1] 55 55
inter4$site_elevation <- as.numeric(as.character(inter4$site_elevation))
inter4$agl <- inter4$altitude - ifelse(inter4$site_elevation == -1e+34, NaN, inter4$site_elevation)

if(nrow(inter4) > 0) fwrite(inter4, "receptors/aircraft_insitu_2020_NORTH_AMERICA.csv")

# add new_variable in x[8]
ggplot(inter4[!is.na(value)],
       aes(x = longitude, 
           y = latitude,
           colour = mole_fraction,
           size = altitude)) +
  geom_point() +
  facet_wrap(~month)+
  labs(title = "aircraft-insitu US-domain 2020", 
       subtitle = paste0("Observations: ", nrow(inter4)))+
  scale_color_gradientn("ppb",colours = cptcity::cpt(4151))+
# scale_color_gradientn(colours = cptcity::lucky())
  theme_dark()
# 550x400

# surface-insitu ####
x1 <- grep(pattern = "surface-insitu", x = x, value = T)
source("rscripts/source_lx.R")
source("rscripts/source_sfv2.R")
inter3 <- inter3[lh %in% 12:18]
inter4 <- inter3

inter4$site_elevation <- as.numeric(as.character(inter4$site_elevation))
inter4$agl <- inter4$altitude - ifelse(inter4$site_elevation == -1e+34, NaN, inter4$site_elevation)
dim(inter4) # [1] 8792   44

if(nrow(inter4) > 0)  fwrite(inter4, "receptors/surface_insitu_2020_NORTH_AMERICA.csv")

# add new_variable at end

ggplot(inter4[!is.na(value),
             mean(mole_fraction, na.rm = T),
             by = .(month, latitude, longitude, altitude)],
       aes(x = longitude, 
           y = latitude,
           colour = V1,
           size = altitude)) +
  geom_point() +
  facet_wrap(~month)+
  labs(title = "surface-insitu US-domain 2020",
       subtitle = paste0("Observations: ", nrow(inter4)))+
  scale_color_gradientn("ppb",colours = cptcity::cpt(4151))+
  # scale_color_gradientn(colours = cptcity::lucky())
  theme_dark() 
# 550x400


# aircore ####
x1 <- grep(pattern = "aircore", x = x, value = T)
source("rscripts/source_lx.R")
source("rscripts/source_sfv2.R")
inter3 <- inter3[lh %in% 12:18]
inter4 <- inter3

inter4$site_elevation <- as.numeric(as.character(inter4$site_elevation))
inter4$agl <- inter4$altitude - ifelse(inter4$site_elevation == -1e+34, NaN, inter4$site_elevation)
dim(inter4) # [1] 8792   40

if(nrow(inter4) > 0) fwrite(inter4, "receptors/aircore_2020_NORTH_AMERICA.csv")

# surface-pfp ####
# done by lei

# tower-insitu ####
x1 <- grep(pattern = "tower-insitu", x = x, value = T)
source("rscripts/source_lx.R")
source("rscripts/source_sfv2.R")
inter3 <- inter3[lh %in% 12:18]
inter4 <- inter3

inter4$site_elevation <- as.numeric(as.character(inter4$site_elevation))
inter4$agl <- inter4$altitude - ifelse(inter4$site_elevation == -1e+34, NaN, inter4$site_elevation)
dim(inter4) # [1] 3683   44

if(nrow(inter4) > 0) fwrite(inter4, "receptors/tower_insitu_2020_NORTH_AMERICA.csv")

# add V1


ggplot(inter[!is.na(value),
             mean(value, na.rm = T),
             by = .(month, latitude, longitude, altitude)],
       aes(x = longitude, 
           y = latitude,
           colour = V1,
           size = altitude)) +
  geom_point() +
  facet_wrap(~month)+
  labs(title = "tower-insitu US-domain 2020")+
  scale_color_gradientn("ppb",colours = cptcity::cpt(4151))+
  # scale_color_gradientn(colours = cptcity::lucky())
  theme_dark() 
# 550x400


# shipboard-insitu ####
x1 <- grep(pattern = "shipboard-insitu", x = x, value = T)
source("rscripts/source_lx.R") # check
dt <- dt[year == 2020 &
           altitude < 8000]
dt

# NON NOAA flask ####
x1 <- grep(pattern = "flask", x = x, value = T)
source("rscripts/source_lx.R")
source("rscripts/source_sfv2.R")
# inter3 <- inter3[lh %in% 12:18]
inter4 <- inter3[lab_1_abbr != "NOAA"]

inter4$site_elevation <- as.numeric(as.character(inter4$site_elevation))
inter4$agl <- inter4$altitude - ifelse(inter4$site_elevation == -1e+34, NaN, inter4$site_elevation)
dim(inter4) # [1] 0 49, No "CSIRO" at 14:00 LT


ggplot(inter4,
       aes(x = longitude, 
           y = latitude,
           colour = mole_fraction,
           size = altitude)) +
  geom_point() +
  facet_wrap(~month)+
  labs(title = "flask Non-NOAA US-domain 2020")+
  scale_color_gradientn("ppb",colours = cptcity::cpt(4151))+
  # scale_color_gradientn(colours = cptcity::lucky())
  theme_dark() 
# 550x400


if(nrow(inter4) > 0) fwrite(inter4, "receptors/flask_no_NOAA_2020_NORTH_AMERICA.csv")

