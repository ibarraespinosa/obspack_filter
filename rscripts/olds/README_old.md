# obspack_filter (Future R package?)

Sergio Ibarra-Espinosa 

This collection of Rscripts reads an obspack database of text files and
filter them accordingly. 

## Dependencies

The following R packages has dependencies:

- `sf`: 	C++11, GDAL (>= 2.0.1), GEOS (>= 3.4.0), PROJ (>= 4.8.0), sqlite3
- `ggplot2` (this is just a cosmetic dependency for some plots)
- `data.table`: C, zlib

This code would run on tsunami2, I have not been able to install dependencies on orion.
However, the best approach to get the outputs would be
clonning the whole directory, run the code in our local machine and upload the outputs.

In tsunami2 I could install these R packages only with conda:

```bash
conda install r-sf "poppler<0.62"
```

Then in R

```R
install.packages(c("ggplot2", 
                   "data.table",
                   "sf",
                   "cptcity",
                   "rnaturalearth",
                   "classInt"))
```
However, I have not tested the code in tsunami2.


## working directory

in this directory tehre are the following sub-directories
```bash
.
├── figures/
├── hysplit_example/
├── master_files/
├── ne_10m_time_zones/
├── receptors/
├── rscripts/
│   └── olds/
└── s_22mr22/

```

## Inputs:

path to obspack: 
1) **"obspack_ch4_1_GLOBALVIEWplus_v4.0_2021-10-14/data/txt"**

2) Shapefile of timezones obtained from https://www.naturalearthdata.com/downloads/10m-cultural-vectors/timezones/. The shapefile is used to identify the local time
whenre the metadata variable `site_utc2lst` is not available.
The repository already have this shapefile:

```bash
s_22mr22/
.
├── s_22mr22.dbf
├── s_22mr22.prj
├── s_22mr22.shp
├── s_22mr22.shx
└── s_22mr22.zip
```



## Steps

### `rscripts/01_read_txt.R` 

reads the text files according the following categories:

- "aircraft-pfp". This category was done by Lei so it is commented in the code.
- "aircraft-insitu".
- "surface-insitu".
- "aircore".
- "surface-pfp". This category was done by Lei so it is commented in the code.
- "tower-insitu".
- "shipboard-insitu".
- "flask". Consider only non-NOAA labs.

```R
source("rscripts/01_read_txt.R")
```

#### parameters

The parameters are hard-codded at the moment, which means that to change the resutlts,
the user must change the code.

Here the user can change:

* the path to GLOBALVIEW, default:
    * "../obspack_ch4_1_GLOBALVIEWplus_v4.0_2021-10-14/data/txt"
* Corner coordinates, defaults are:
    * north = 80
    * south = 10
    * west = -170
    * east = -50
* selected hours, defaults:
    * `12:18` to all variables, 
    * For `flask` all hours
* frequency of seconds for aircraft, default is `5, 25, and 45`
* output "receptors/aircraft_insitu_2020_NORTH_AMERICA.csv")

Then, this script source `rscripts/source_lx.R`  and `rscripts/source_sfv2.R`,
explained in the following sections, and add the columns:

- `site_elevation`
- `agl`

IF the resulting databse has observations, they are stored in as:

- `receptors/*2020_NORTH_AMERICA.csv`

#### `rscripts/source_lx.R` 

This script iterates in all Osbapack first finding all the files that matches with a criteria,
for instance: `aircraft-insitu`. Then, the script reads the metadata
and add them as new columns. Currently, the new metadata columns added are:

- `site_code`
- `site_name`
- `site_country`
- `dataset_project`
- `lab_1_abbr` (laboratory)
- `elevation`
- `altitude_comment`
- `site_utc2lst`

This script also transform the column time into UTC time with the new columns:

- timeUTC

#### `rscripts/source_sfv2.R`

1. Firstly, this script filter observations for the year 2020 and altitude < 8000m.
2. Second, this script identify all observations with valkues in the column
`site_utc2lst`. 
3. Third, in case of missing values on the `site_utc2lst`, it 
 calculates the local time based on the intersection
between the lat-long points and the timezone areas. Then, calculate local time
adding the following columns:

- `mole_fraction`, which is the observed `value` times *1e9 to convert to ppb
- `decimal_year`, which just is  dt$time_decimal
- `local_time`
- `lh` (local hour)
- `year_localtime`
- `month_localtime`
- `day_localtime`
- `hour_localtime`

### `rscripts/02_master_files.R` 

This script reads all the files in the `receptors` diretory that has the
pattern `NORTH_AMERICA`, select the columns shown below
and calculate add the new column `altitude_type`, "masl" or "agl"


year, month, day, hour, minute, second, time, time_decimal,
value, value_std_dev, value_unc, nvalue,
latitude,     longitude,      altitude,       pressure,
obs_flag,     obs_num,        obspack_num,    obspack_id,
site_code,    site_name,      site_country,   dataset_project,
lab_1_abbr,   site_elevation, altitude_type (asl or agl),
site_utc2lst, source_id,      time_interval,  timeUTC,
local_time,   year_localtime, month_localtime,
day_localtime,hour_localtime, file,   elevation,
intake_height,qcflag,         altitude_type

The, it stores each files in the directory `master_files`, 
The directory `master_files` has outputs with extensions .csv and .txt.
The .csv files has blank spaces for missing values, then as the are separated
by ",", any csv reader could spott the missing values.

the .txt files replaces the missing values with a character "NAN" for Not Available.

```bash
master_files
.
├── aircore_2020_NORTH_AMERICA.csv
├── aircore_2020_NORTH_AMERICA.txt
├── aircraft_insitu_2020_NORTH_AMERICA.csv
├── aircraft_insitu_2020_NORTH_AMERICA.txt
├── all.csv
├── flask_no_NOAA_2020_NORTH_AMERICA.csv
├── flask_no_NOAA_2020_NORTH_AMERICA.txt
├── surface_insitu_2020_NORTH_AMERICA.csv
├── surface_insitu_2020_NORTH_AMERICA.txt
├── tower_insitu_2020_NORTH_AMERICA.csv
└── tower_insitu_2020_NORTH_AMERICA.txt
```

### `rscripts/03_receptors.R` 

This script reads all the files in the `receptors` diretory that has the
pattern `NORTH_AMERICA`. Then, it following columns:

- `file`
- `minutes_localtime`
- `seconds_localtime`

Then, this code format the time variables, so that they start with 0, 
when the character number is lower than 2, for instance, 2 goes to 02.
Then, based on the toy file `receptor_test.txt`,
it select the matching variables and rename them as:

- `# sample_site_code`
- `sample_year`
- `sample_month`
- `sample_day`
- `sample_hour`
- `sample_minute`
- `sample_seconds`
- `sample_id`
- `sample_method`
- `sample_latitude`
- `sample_longitude`
- `sample_altitude`
- `sample_elevation`
- `event_number`
- `mole_fraction`
- `decimal_year`

and stores the results in the receptors directory as files with `.txt` extension.


```bash
receptors
.
├── ccg_aircraft_2020.txt
├── ccg_flask_2020.txt
├── ch4_aircore_2020.txt
├── ch4_aircraft_2020.txt
├── ch4_NO_NOAA_flask_2020.txt
├── ch4_surface_2020.txt
├── ch4_tower_2020.txt
├── hats_flask_2020.txt
└── receptor_test.txt
```

### `rscripts/04_plots.R` 

generates a plot under `figures/obspack.png`

